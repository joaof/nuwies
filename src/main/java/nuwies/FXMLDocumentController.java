/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nuwies;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.converter.NumberStringConverter;
import nuwies.impl.Context;
import nuwies.impl.DownloadTask;

/**
 *
 * @author joaof
 */
public class FXMLDocumentController {

    Context context = new Context();

    @FXML
    protected void initialize() {
        setBindings();
    }

    @FXML
    private Label filenameLbl;

    @FXML
    private Label imgFilenameLbl;

    @FXML
    private TextField startEdt;

    @FXML
    private TextField refererEdt;

    @FXML
    private TextField destinationDirEdt;

    @FXML
    private TextField endEdt;

    @FXML
    private TextField urlEdt;

    @FXML
    private ImageView imgViewer;

    @FXML
    private Pane imagePane;

    @FXML
    private Button startBtn;

    @FXML
    private Button pasteDestDirBtn;

    @FXML
    private Button pasteNameBtn;

    @FXML
    private Button pasteRefererBtn;

    @FXML
    private Button clearDestDirBtn;

    @FXML
    private Button clearNameBtn;

    @FXML
    private Button clearRefererBtn;

    @FXML
    private CheckBox useRefererChb;

    @FXML
    private TextField nameEdt;
    
    @FXML
    private Label currentOnTotalLbl;

    Preferences prefs = Preferences.userNodeForPackage(FXMLDocumentController.class);

    @FXML
    private void selDestDirAction(ActionEvent event) {
        DirectoryChooser dc = new DirectoryChooser();
        File d = dc.showDialog(null);
        if (d != null) {
            context.setDestinationDir(d.getAbsolutePath());
        }
    }

    @FXML
    private void loadAction(ActionEvent event) {
        FileChooser fc = new FileChooser();
        String prefPath = prefs.get(PREFS_DEFAULT_LOAD_SAVE_PATH, null);
        if (prefPath == null || !new File(prefPath).exists() || !new File(prefPath).isDirectory()) {
            prefPath = System.getProperty("user.home");
        }
        fc.setInitialDirectory(new File(prefPath));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Nuwies files", "*.nuwies"));
        File f;
        if ((f = fc.showOpenDialog(imgViewer.getScene().getWindow())) != null) {
            try {
                Context.read(context, f.getAbsolutePath());
            } catch (Exception ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                var alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error opening file");
                alert.setHeaderText(MessageFormat.format("Failed to open file {0}", f.getName()));
                alert.setContentText(ex.getMessage());
                alert.showAndWait();
            }
            context.setFilename(f.getAbsolutePath());
        }
    }
    private static final String PREFS_DEFAULT_LOAD_SAVE_PATH = "defaultLoadSavePath";

    private static final String PREFS_PERIOD = "waitPeriod";

    public int getWaitPeriodMs() {
        return prefs.getInt(PREFS_PERIOD, 1500);
    }

    @FXML
    private void saveAction(ActionEvent event) {
        File f;
        if (context.getFilename() != null) {
            f = new File(context.getFilename());
        } else {
            FileChooser fc = new FileChooser();
            fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Nuwies files", "*.nuwies"));
            if ((f = fc.showSaveDialog(imgViewer.getScene().getWindow())) == null) {
                return;
            }
        }
        try {
            Context.write(context, f.getAbsolutePath());
            context.setFilename(f.getAbsolutePath());
            prefs.put(PREFS_DEFAULT_LOAD_SAVE_PATH, f.getAbsoluteFile().getParent());
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.WINDOW_MODAL);
            VBox vb = new VBox();
            vb.getChildren().add(new Text(sw.toString()));
            vb.getChildren().add(new Button("Ok"));
            vb.setAlignment(Pos.CENTER);
            vb.setPadding(new Insets(5));
            dialogStage.setScene(new Scene(vb));
            dialogStage.show();
        }
    }

    private boolean started;

    private void downloadStarted() {
        started = true;
        startBtn.setText("Stop");
    }

    private void downloadEnded() {
        started = false;
        startBtn.setText("Start");
    }

    @FXML
    private void quitButtonAction(ActionEvent event) {
        System.exit(0);
    }

    ScheduledService<Void> sched;

    @FXML
    private void startStopButtonAction(ActionEvent event) {
        if (!started) {
            if (!canStart()) {
                return;
            }
            context.setNext(context.getStart());
            sched = new ScheduledService<Void>() {
                @Override
                protected Task<Void> createTask() {
                    return new DownloadTask(context, FXMLDocumentController.this);
                }

                @Override
                protected void succeeded() {
                    int count = context.getEnd() - context.getStart() + 1;
                    int current = context.getNext() - context.getStart() + 1;
                    currentOnTotalLbl.setText(String.format("%d / %d", current, count));
                    if (context.getEnd().intValue() == context.getNext().intValue()) {
                        cancel();
                    } else {
                        context.setNext(context.getNext() + 1);
                    }
                    super.succeeded();
                }

                @Override
                protected void cancelled() {
                    downloadEnded();
                }
            };
            sched.setPeriod(Duration.millis(getWaitPeriodMs()));
            sched.start();
            downloadStarted();
        } else {
            startBtn.setText("Start");
            sched.cancel();
        }
    }

    @FXML
    void pasteButtonAction(ActionEvent event) {
        String data;
        try {
            data = (String) Toolkit.getDefaultToolkit()
                    .getSystemClipboard().getData(DataFlavor.stringFlavor);
        } catch (UnsupportedFlavorException | IOException ex) {
            data = "";
        }

        if (event.getSource() == pasteDestDirBtn) {
            destinationDirEdt.textProperty().setValue(data);
        } else if (event.getSource() == pasteNameBtn) {
            nameEdt.textProperty().setValue(data);
        } else if (event.getSource() == pasteRefererBtn) {
            refererEdt.textProperty().setValue(data);
        }
    }

    @FXML
    void clearButtonAction(ActionEvent event) {
        if (event.getSource() == clearDestDirBtn) {
            destinationDirEdt.textProperty().setValue("");
        } else if (event.getSource() == clearNameBtn) {
            nameEdt.textProperty().setValue("");
        } else if (event.getSource() == clearRefererBtn) {
            refererEdt.textProperty().setValue("");
        }
    }

    private void setBindings() {
        startEdt.textProperty().bindBidirectional(context.startProperty(), new NumberStringConverter());
        endEdt.textProperty().bindBidirectional(context.endProperty(), new NumberStringConverter());
        urlEdt.textProperty().bindBidirectional(context.urlProperty());
        destinationDirEdt.textProperty().bindBidirectional(context.destinationDirProperty());

        filenameLbl.textProperty().bind(context.filenameProperty());
        imgViewer.fitWidthProperty().bind(imagePane.widthProperty());
        imgViewer.fitHeightProperty().bind(imagePane.heightProperty());
        imgViewer.imageProperty().bind(context.imageProperty());
        destinationDirEdt.textProperty().bindBidirectional(context.destinationDirProperty());
        imgFilenameLbl.textProperty().bind(context.imgFilePathProperty());

        useRefererChb.selectedProperty().bindBidirectional(context.useRefererProperty());
        refererEdt.textProperty().bindBidirectional(context.refererProperty());
        BooleanBinding not = Bindings.not(context.useRefererProperty());
        refererEdt.disableProperty().bind(not);
        pasteRefererBtn.disableProperty().bind(not);
        clearRefererBtn.disableProperty().bind(not);
        nameEdt.textProperty().bindBidirectional(context.nameProperty());
    }

    private boolean canStart() {
        Path destPath = Path.of(destinationDirEdt.textProperty().get());
        if (!Files.exists(destPath)) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, String.format("Directory %s does not exists. Create ?", destPath));
            var res = alert.showAndWait();
            if (res.isEmpty() || res.get() == ButtonType.CANCEL) {
                return false;
            }
            try {
                Files.createDirectory(destPath);
            } catch (IOException ex) {
                new Alert(Alert.AlertType.ERROR, String.format("Failed to create directorory %s", destPath)).show();
                return false;
            }
        }
        return true;
    }

}
