/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nuwies.impl;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;

/**
 *
 * @author joaof
 */
public class Context {

    private final IntegerProperty start = new SimpleIntegerProperty();

    public Integer getStart() {
        return startProperty().get();
    }

    public void setStart(Integer start) {
        this.startProperty().set(start);
    }

    public IntegerProperty startProperty() {
        return start;
    }

    private final IntegerProperty end = new SimpleIntegerProperty();

    public Integer getEnd() {
        return endProperty().get();
    }

    public void setEnd(Integer end) {
        this.endProperty().set(end);
    }

    public IntegerProperty endProperty() {
        return end;
    }

    private final IntegerProperty next = new SimpleIntegerProperty();

    public Integer getNext() {
        return nextProperty().get();
    }

    public void setNext(Integer next) {
        this.nextProperty().set(next);
    }

    public IntegerProperty nextProperty() {
        return next;
    }

    private final StringProperty url = new SimpleStringProperty();

    public String getUrl() {
        return urlProperty().get();
    }

    public void setUrl(String url) {
        this.urlProperty().set(url);
    }

    public StringProperty urlProperty() {
        return url;
    }

    private final ObjectProperty<Image> image = new SimpleObjectProperty<Image>() {
    };

    public Image getImage() {
        return image.get();
    }

    public void setImage(Image image) {
        this.image.set(image);
    }

    public ObjectProperty<Image> imageProperty() {
        return image;
    }

    public static void write(Context c, String filename) throws Exception {
        write(c, new FileOutputStream(filename));
    }

    public static void write(Context c, OutputStream out) throws Exception {
        try (XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(out))) {
            encoder.writeObject(c);
        }
    }

    public static void read(Context c, String filename) throws Exception {
        read(c, new FileInputStream(filename));
    }

    public static void read(Context c, InputStream in) throws Exception {
        Context o;
        try (XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(in), c)) {
            o = (Context) decoder.readObject();
            o.copyTo(c);
        }
    }

    private final StringProperty filenameProperty = new SimpleStringProperty();

    public String getFilename() {
        return filenameProperty().get();
    }

    public void setFilename(String url) {
        this.filenameProperty().set(url);
    }

    public StringProperty filenameProperty() {
        return filenameProperty;
    }

    private void copyTo(Context c) {
        c.setEnd(getEnd());
        c.setStart(getStart());
        c.setFilename(getFilename());
        c.setUrl(getUrl());
        c.setDestinationDir(getDestinationDir());
        c.setUseReferer(getUseReferer());
        c.setReferer(getReferer());
        c.setName(getName());
    }

    private final StringProperty destinationDir = new SimpleStringProperty();

    public String getDestinationDir() {
        return destinationDir.get();
    }

    public void setDestinationDir(String dest) {
        this.destinationDir.set(dest);
    }

    public StringProperty destinationDirProperty() {
        return destinationDir;
    }

    private final StringProperty imgFilePath = new SimpleStringProperty();

    public String getImgFilePath() {
        return imgFilePath.get();
    }

    public void setImgFilePath(String imgFilePath) {
        this.imgFilePath.set(imgFilePath);
    }

    public StringProperty imgFilePathProperty() {
        return imgFilePath;
    }

    private final BooleanProperty useReferer = new SimpleBooleanProperty();

    public BooleanProperty useRefererProperty() {
        return useReferer;
    }

    public Boolean getUseReferer() {
        return useRefererProperty().get();
    }

    public void setUseReferer(Boolean useReferer) {
        this.useRefererProperty().set(useReferer);
    }

    private final StringProperty referer = new SimpleStringProperty();

    public String getReferer() {
        return referer.get();
    }

    public void setReferer(String referer) {
        this.referer.set(referer);
    }

    public StringProperty refererProperty() {
        return referer;
    }

    private final StringProperty name = new SimpleStringProperty();

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

}
