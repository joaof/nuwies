/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nuwies;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.image.Image;

/**
 *
 * @author joaof
 */
public class DownloadTask extends Task<Void> {

    private final Context context;
    private final FXMLDocumentController controller;

    public DownloadTask(Context context, FXMLDocumentController controller) {
        this.context = context;
        this.controller = controller;
    }

    private void downloadUrl(URL url, String file) throws IOException {
        final URLConnection cnx = url.openConnection();
        if (context.getUseReferer() && (context.getReferer()) != null && ! (context.getReferer().isEmpty())) {
            cnx.setRequestProperty("Referer", context.getReferer());
        }
        try (ReadableByteChannel rbc = Channels.newChannel(cnx.getInputStream()); FileOutputStream fos = new FileOutputStream(file)) {
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        }
    }

    /**
     * The actual file name to save to. If the name is extract the name from the 
     * URL. Otherwise use the name.
     * @return 
     */
    private String actualFileName()
    {
        String actual = context.getName();
        if ((actual == null) || actual.trim().isEmpty()) {
            try {
                actual = new File(new URL(context.getUrl()).getFile()).getName();
            } catch (MalformedURLException ex) {
                Logger.getLogger(DownloadTask.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return actual;
    }
    
    @Override
    protected Void call() throws Exception {
        Platform.runLater(() -> {
            controller.downloadStarted();
        });
        for (int i = context.getStart(); i <= context.getEnd(); i++) {
            if (controller.getDoStop())
                break;
            try {
                logger.info(String.format("Start downloading %d", i));
                URL url = new URL(String.format(context.getUrl(), i));
                final String fname = String.format("%1s/" + actualFileName(), context.getDestinationDir(), i);
                downloadUrl(url, fname);
                Platform.runLater(() -> {
                    context.setImage(new Image(new File(fname).toURI().toString()));
                    context.setImgFilePath(fname);
                });
                logger.log(Level.INFO,
                        String.format("Downloaded %s to %s", url.toString(), fname));
                Thread.sleep(controller.getWaitPeriodMs());
            } catch (IOException | InterruptedException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
        Platform.runLater(() -> {
            controller.downloadEnded();
        });
        return null;
    }
    private static final Logger logger = Logger.getLogger(DownloadTask.class.getName());

}
