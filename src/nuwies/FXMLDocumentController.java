/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nuwies;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;

/**
 *
 * @author joaof
 */
public class FXMLDocumentController implements Initializable {

    Context context = new Context();

    @FXML
    private Label filenameLbl;

    @FXML
    private Label imgFilenameLbl;

    @FXML
    private TextField startEdt;

    @FXML
    private TextField refererEdt;

    @FXML
    private TextField destinationDirEdt;

    @FXML
    private TextField endEdt;

    @FXML
    private TextField urlEdt;

    @FXML
    private ImageView imgViewer;

    @FXML
    private Pane imagePane;

    @FXML
    private Button startBtn;
    
    @FXML
    private Button pasteDestDirBtn;
    
    @FXML
    private Button pasteNameBtn;
    
    @FXML
    private Button pasteRefererBtn;
    
    @FXML
    private Button clearDestDirBtn;
    
    @FXML
    private Button clearNameBtn;
    
    @FXML
    private Button clearRefererBtn;
    
    @FXML
    private CheckBox useRefererChb;
    
    @FXML
    private TextField nameEdt;
    
    private boolean doStop;
    
    Preferences prefs = Preferences.userNodeForPackage(FXMLDocumentController.class);

    @FXML
    private void selDestDirAction(ActionEvent event) {
        DirectoryChooser dc = new DirectoryChooser();
        File d = dc.showDialog(null);
        if (d != null) {
            context.setDestinationDir(d.getAbsolutePath());
        }
    }

    @FXML
    private void loadAction(ActionEvent event) {
        FileChooser fc = new FileChooser();
        String prefPath = prefs.get(PREFS_DEFAULT_LOAD_SAVE_PATH, null);
        if (prefPath == null || !new File(prefPath).exists() || !new File(prefPath).isDirectory()) {
            prefPath = System.getProperty("user.home");
        }
        fc.setInitialDirectory(new File(prefPath));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Nuwies files", "*.nuwies"));
        File f;
        if ((f = fc.showOpenDialog(imgViewer.getScene().getWindow())) != null) {
            try {
                Context.read(context, f.getAbsolutePath());
                context.setFilename(f.getAbsolutePath());
            } catch (Exception e) {
                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.WINDOW_MODAL);
                VBox vb = new VBox();
                vb.getChildren().add(new Text(sw.toString()));
                vb.getChildren().add(new Button("Ok"));
                vb.setAlignment(Pos.CENTER);
                vb.setPadding(new Insets(5));
                dialogStage.setScene(new Scene(vb));
                dialogStage.show();
            }
        }
    }
    private static final String PREFS_DEFAULT_LOAD_SAVE_PATH = "defaultLoadSavePath";
    
    private static final String PREFS_PERIOD = "waitPeriod";
    
    public int getWaitPeriodMs() {
        return prefs.getInt(PREFS_PERIOD, 1500);
    }

    @FXML
    private void saveAction(ActionEvent event) {
        File f;
        if (context.getFilename() != null) {
            f = new File(context.getFilename());
        } else {
            FileChooser fc = new FileChooser();
            fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Nuwies files", "*.nuwies"));
            if ((f = fc.showSaveDialog(imgViewer.getScene().getWindow())) == null) {
                return;
            }
        }
        try {
            Context.write(context, f.getAbsolutePath());
            context.setFilename(f.getAbsolutePath());
            prefs.put(PREFS_DEFAULT_LOAD_SAVE_PATH, f.getAbsoluteFile().getParent());
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.WINDOW_MODAL);
            VBox vb = new VBox();
            vb.getChildren().add(new Text(sw.toString()));
            vb.getChildren().add(new Button("Ok"));
            vb.setAlignment(Pos.CENTER);
            vb.setPadding(new Insets(5));
            dialogStage.setScene(new Scene(vb));
            dialogStage.show();
        }
    }

    private boolean started;

    public void downloadStarted() {
        started = true;
    }

    public void downloadEnded() {
        started = false;
    }

    @FXML
    private void quitButtonAction(ActionEvent event) {
        System.exit(0);
    }
    
    @FXML
    private void startButtonAction(ActionEvent event) {
        if (!started) {
            doStop = false;
            DownloadTask dt = new DownloadTask(context, this);
            Thread t = new Thread(dt, "DOWNLOADTHREAD");
            t.setDaemon(true);
            t.start();
            startBtn.setText("Stop");
        } else {
            doStop = true;
            startBtn.setText("Start");
        }
    }
    
    @FXML void pasteButtonAction(ActionEvent event) {
        String data = "";
        try {
            data = (String) Toolkit.getDefaultToolkit() 
                    .getSystemClipboard().getData(DataFlavor.stringFlavor);
        } catch (UnsupportedFlavorException | IOException ex) {
            data = "";
        }

        if (event.getSource() == pasteDestDirBtn) {
            destinationDirEdt.textProperty().setValue(data);
        }
        else if (event.getSource() == pasteNameBtn) {
            nameEdt.textProperty().setValue(data);
        }
        else if (event.getSource() == pasteRefererBtn) {
            refererEdt.textProperty().setValue(data);
        }
    }

    @FXML void clearButtonAction(ActionEvent event) {
        if (event.getSource() == clearDestDirBtn) {
            destinationDirEdt.textProperty().setValue("");
        }
        else if (event.getSource() == clearNameBtn) {
            nameEdt.textProperty().setValue("");
        }
        else if (event.getSource() == clearRefererBtn) {
            refererEdt.textProperty().setValue("");
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    void setBindings() {
        startEdt.textProperty().bindBidirectional(context.startProperty(), new NumberStringConverter());
        endEdt.textProperty().bindBidirectional(context.endProperty(), new NumberStringConverter());
        urlEdt.textProperty().bindBidirectional(context.urlProperty());
        destinationDirEdt.textProperty().bindBidirectional(context.destinationDirProperty());

        filenameLbl.textProperty().bind(context.filenameProperty());
        imgViewer.fitWidthProperty().bind(imagePane.widthProperty());
        imgViewer.fitHeightProperty().bind(imagePane.heightProperty());
        imgViewer.imageProperty().bind(context.imageProperty());
        destinationDirEdt.textProperty().bindBidirectional(context.destinationDirProperty());
        imgFilenameLbl.textProperty().bind(context.imgFilePathProperty());
        
        useRefererChb.selectedProperty().bindBidirectional(context.useRefererProperty());
        refererEdt.textProperty().bindBidirectional(context.refererProperty());
        BooleanBinding not = Bindings.not(context.useRefererProperty());
        refererEdt.disableProperty().bind(not);
        pasteRefererBtn.disableProperty().bind(not);
        clearRefererBtn.disableProperty().bind(not);
        nameEdt.textProperty().bindBidirectional(context.nameProperty());
    }

    boolean getDoStop() {
        return doStop;
    }

}
